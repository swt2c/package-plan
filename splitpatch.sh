#!/bin/bash

file=$1
patch=$2


hunks=$(grep ^@@ $patch|wc -l)

echo "Detected $hunks hunks"

cp $file $file.orig

sel=

for n in $(seq 1 $hunks)
do
cp $file.orig $file
filterdiff -# $n < $patch | patch --quiet $file

if schroot -c haskell-exp -- ./test-packages.pl >/dev/null
then
  echo "Hunk $n is usable on its own"
  ok="$ok $n"
else
  echo "Hunk $n is not usable on its own"
fi
done
cp $file.orig $file

echo Usable hunks: $(echo $ok|tr ' ' ,)
